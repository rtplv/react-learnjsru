import React, { Component } from 'react';
// store
import { Provider } from 'react-redux';
import store from './store';
//views
import ArticlesView from './containers/ArticlesView';

class Root extends Component {
  render() {
    return (
      <Provider store={store}>
        <ArticlesView />
      </Provider>
    );
  }
}

export default Root;
