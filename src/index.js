import React from 'react';
import ReactDOM from 'react-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import './css/animation.css';
import './css/validation.css';

import Root from './Root';

ReactDOM.render(<Root />, document.getElementById('root'));
