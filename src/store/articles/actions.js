import * as actionTypes from './actionTypes';

export function setSelectedArticles(selectedArticles) {
  return {
    type: actionTypes.SET_SELECTED_ARTICLES,
    payload: { selectedArticles }
  }
}

export function setDateRange(range) {
  return {
    type: actionTypes.SET_DATE_RANGE,
    payload: { range }
  }
}

export function removeArticle(id) {
  return {
    type: actionTypes.REMOVE_ARTICLE,
    payload: { id }
  }
}
