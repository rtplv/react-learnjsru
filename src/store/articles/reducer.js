import * as actionTypes from './actionTypes';
import defaultArticles from '../../mocks/articles';

const initialState = {
  list: defaultArticles,
  filters: {
    selected: [],
    dateRange: {
      from: null,
      to: null
    }
  }
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch(type) {
    case actionTypes.REMOVE_ARTICLE:
      const filteredList = state.list.filter(article => article.id !== payload.id);
      return {...state, changedList: filteredList};
    case actionTypes.SET_SELECTED_ARTICLES:
      return {
        ...state,
        filters: {
          ...state.filters,
          selected: payload.selectedArticles
        }
      };
    case actionTypes.SET_DATE_RANGE:
      return {
        ...state,
        filters: {
          ...state.filters,
          dateRange: payload.range
        }
      };
    default:
      return state;
  }
}
