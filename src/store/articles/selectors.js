import { createSelector } from 'reselect';

const articlesGetter = state => state.articles.list;
const filtersGetter = state => state.articles.filters;

export const filteredArticlesBySelected = createSelector(articlesGetter, filtersGetter, (articles, filters) => {
  const { selected } = filters;

  if(selected.length !== 0) {
    return articles.filter(article => {
      return selected.findIndex(option => article.id  === option.value) !== -1;
    });
  }

  return articles;
});

export const filteredArticlesByDateRange = createSelector(
  [filteredArticlesBySelected, filtersGetter],
  (articles, filters) => {
  const { dateRange } = filters;
  let filtered = articles;

  if(dateRange.from) {
    filtered = filtered.filter(article => {
      const dateCreated = new Date(article.date_created);
      return dateCreated.getTime() >= dateRange.from.getTime();
    });
  }
  if(dateRange.to) {
    filtered = filtered.filter(article => {
      const dateCreated = new Date(article.date_created);
      return dateCreated.getTime() <= dateRange.to.getTime();
    });
  }

  return filtered;
});

export const filteredArticles = createSelector([filteredArticlesByDateRange], filteredArticles => filteredArticles);
