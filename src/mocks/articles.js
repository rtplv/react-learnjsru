export default [
  {
    id: 1,
    title: 'Новость 1',
    text: 'Текст новости 1',
    date_created: "2018-09-11T08:00:00.000Z",
    comments: []
  },
  {
    id: 2,
    title: 'Новость 2',
    text: 'Текст новости 2',
    date_created: "2018-09-01T08:00:00.000Z",
    comments: [
      {
        id: 1,
        text: 'Коммент 1'
      }
    ]
  },
  {
    id: 3,
    title: 'Новость 3',
    date_created: "2018-09-15T08:00:00.000Z",
    text: 'Текст новости 3',
    comments: []
  },
  {
    id: 4,
    title: 'Новость 4',
    date_created: "2018-09-20T08:00:00.000Z",
    text: 'Текст новости 4',
    comments: []
  },
  {
    id: 5,
    title: 'Новость 5',
    date_created: "2018-09-21T08:00:00.000Z",
    text: 'Текст новости 5',
    comments: [
      {
        id: 1,
        text: 'Коммент 1'
      },
      {
        id: 2,
        text: 'Коммент 2'
      }
    ]
  },
  {
    id: 6,
    title: 'Новость 6',
    date_created: "2018-10-01T08:00:00.000Z",
    text: 'Текст новости 6',
    comments: []
  },
  {
    id: 7,
    title: 'Новость 7',
    date_created: "2018-10-04T08:00:00.000Z",
    text: 'Текст новости 7',
    comments: [
      {
        id: 1,
        text: 'Коммент 1'
      },
      {
        id: 2,
        text: 'Коммент 2'
      },
      {
        id: 3,
        text: 'Коммент 3'
      },
      {
        id: 4,
        text: 'Коммент 4'
      }
    ]
  },
  {
    id: 8,
    title: 'Новость 8',
    date_created: "2018-10-06T08:00:00.000Z",
    text: 'Текст новости 8',
    comments: []
  },
  {
    id: 9,
    title: 'Новость 9',
    date_created: "2018-10-09T08:00:00.000Z",
    text: 'Текст новости 9',
    comments: []
  },
]
