import React, { Component } from  'react';

export default (OriginalComponent) => class AccordionDecorator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openedItemId: props.defaultOpenId,
    };
  }

  toggleOpenArticle = (openedItemId, e) => () => {
    e && e.preventDefault();

    if(this.state.openedItemId === openedItemId){
      return this.setState({ openedItemId: null });
    }

    this.setState({ openedItemId })
  };

  render() {
    return (
      <OriginalComponent {...this.props}
                         openedItemId={this.state.openedItemId}
                         toggleOpen={this.toggleOpenArticle} />
    )
  }
}

