import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';

import toggleOpenDecorator from '../../decorators/toggleOpen';

import Comment from './Comment';
import CommentAddForm from './CommentAddForm';

class CommentsList extends Component {
  static propTypes = {
    comments: PropTypes.array,
    isOpen: PropTypes.bool,
    toggleOpen: PropTypes.func.isRequired
  };

  commentsList() {
    const { comments, isOpen } = this.props;
    if(!isOpen) return null;
    return comments.map(comment => (
      <Comment comment={comment} key={comment.id}/>
    ));
  };

  toggleCommentsShowBtn() {
    const { comments, isOpen, toggleOpen } = this.props;
    if(comments && comments.length > 0) {
      return (
        <Button color="secondary"
                size="sm"
                outline
                onClick={toggleOpen}>
          {isOpen ? 'Скрыть' : 'Показать'} комментарии
        </Button>
      )
    }
  }

  render() {
    return (
      <React.Fragment>
        {this.toggleCommentsShowBtn()}
        <ul className="CommentsList">
          {this.commentsList()}
        </ul>
        <CommentAddForm />
      </React.Fragment>
    );
  }
}

export default toggleOpenDecorator(CommentsList);
