import React, { Component } from 'react';

import PropTypes from 'prop-types';

class Comment extends Component {
  static propTypes = {
    comment: PropTypes.shape({
      id: PropTypes.number.isRequired,
      text: PropTypes.string,
    })
  };

  render() {
    const { comment } = this.props;
    return (
      <li className="Comment">
        {comment.text}
      </li>
    );
  }
}

export default Comment;
