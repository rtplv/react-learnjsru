import React, { Component } from 'react';
import { Form, FormGroup, Label, Input, Button } from 'reactstrap';

const limits = {
  username: {
    min: 5,
    max: 20,
  },
  text: {
    min: 20,
    max: 50,
  },
};

class CommentAddForm extends Component {
  state = {
    username: {
      value: '',
      isValid: true,
    },
    text: {
      value: '',
      isValid: true,
    },
  };

  onFormInputChange = e => {
    const target = e.target;
    const { name, value } = target;

    if(limits[name] && 'min' in limits[name] && 'max' in limits[name]) {
      this.setState({
        [name]: {
          value,
          isValid: value.length > limits[name].min && value.length < limits[name].max,
        }
      });
    } else {
      throw Error('Field require limits min and max value for validation.');
    }
  };

  onFormSubmit = e => {
    e && e.preventDefault();
    const { username, text } = this.state;

    if(username.isValid && text.isValid) {
      console.log('Успешно!', {
        username: username.value,
        text: text.value,
      })
    }
  };

  render() {
    const { username, text } = this.state;
    return (
      <div className="CommentAddForm">
        <Form onSubmit={this.onFormSubmit}>
          <FormGroup>
            <Label for="commentAddName">Ваше имя</Label>
            <Input type="text"
                   name="username"
                   id="commentAddUserName"
                   className={!username.isValid ? 'has-error' : null}
                   placeholder="Фамилия и имя"
                   required={true}
                   value={username.value}
                   onChange={this.onFormInputChange}/>
            {!username.isValid && (
              <span className="has-error__description">
                Укажите имя длинной больше 5 и меньше 20 символов.
              </span>
            )}
          </FormGroup>
          <FormGroup>
            <Label for="commentAddName">Текст комментария</Label>
            <Input type="textarea"
                   name="text"
                   id="commentAddText"
                   className={!text.isValid ? 'has-error' : null }
                   placeholder="Не более 50 символов"
                   required={true}
                   value={text.value}
                   onChange={this.onFormInputChange} />
            {!text.isValid && (
              <span className="has-error__description">
                Укажите комментарий длинной больше 20 и меньше 50 символов.
              </span>
            )}
          </FormGroup>
          <Button type="submit"
                  color="success">
            Добавить
          </Button>
        </Form>
      </div>
    );
  }
}

export default CommentAddForm;
