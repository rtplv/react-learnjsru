import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ReactDayPicker, { DateUtils } from 'react-day-picker'
import 'react-day-picker/lib/style.css';

import MomentLocaleUtils from 'react-day-picker/moment';
import 'moment/locale/ru';

class DayRangeFilter extends Component{
  static propTypes = {
    range: PropTypes.shape({
      from: PropTypes.object,
      to: PropTypes.object,
    }),
    onDayClick: PropTypes.func
  };

  onDayClick = day => {
    const { range, onDayClick } = this.props;
    onDayClick(DateUtils.addDayToRange(day, range));
  };

  render() {
    const { from, to } = this.props.range;
    return (
      <ReactDayPicker {...this.props}
                      selectedDays={[from, to]}
                      onDayClick={this.onDayClick}
                      locale="ru"
                      localeUtils={MomentLocaleUtils}/>
    )
  };
}

export default DayRangeFilter;
