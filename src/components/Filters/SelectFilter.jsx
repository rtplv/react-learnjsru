import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';

class SelectFilter extends Component {
  static propTypes = {
    selected: PropTypes.array,
    onSelected: PropTypes.func,
    options: PropTypes.array,
  };

  onSelectedArticle = options => {
    this.props.onSelected(options);
  };

  render() {
    const { options, selected } = this.props;
    return (
      <Select options={options}
              value={selected}
              onChange={this.onSelectedArticle}
              isMulti />
    );
  }
}

export default SelectFilter;
