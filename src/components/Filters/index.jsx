import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setSelectedArticles, setDateRange } from '../../store/articles/actions'

import { Col } from 'reactstrap';

import PropTypes from 'prop-types';
import moment from 'moment';

import DayRangeFilter from './DayRangeFilter';
import SelectFilter from './SelectFilter';

class Filters extends Component {
  static propTypes = {
    articles: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string,
      id: PropTypes.number,
    })),
    selectedArticles: PropTypes.array,
    dateRange: PropTypes.shape({
      from: PropTypes.object,
      to: PropTypes.object,
    })
  };

  getSelectOptions = () => {
    return this.props.articles.map(article => ({
      label: article.title,
      value: article.id
    }));
  };

  convertDate = date => moment(date).format('L');

  render() {
    const { selectedArticles, setSelectedArticles, dateRange, setDateRange } = this.props;
    return (
      <React.Fragment>
        <Col lg={6}>
          <DayRangeFilter range={dateRange}
                          onDayClick={setDateRange}/>
          <div className="selected-date">
            <span className="selected-date__from">
              {!dateRange.from ? 'Выберите начальную дату ' : `Начальная дата - ${this.convertDate(dateRange.from)} `}
            </span>
            <span className="selected-date__to">
              {!dateRange.to ? ' Выберите конечную дату' : ` Конечная дата - ${this.convertDate(dateRange.to)}`}
            </span>
          </div>
        </Col>
        <Col lg={6}>
          <SelectFilter selected={selectedArticles}
                        options={this.getSelectOptions()}
                        onSelected={setSelectedArticles}/>
        </Col>
      </React.Fragment>
    );
  }
}

export default connect(state => ({
  selectedArticles: state.articles.filters.selected,
  dateRange: state.articles.filters.dateRange,
}), {
  setSelectedArticles,
  setDateRange,
})(Filters);
