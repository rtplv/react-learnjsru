import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { removeArticle } from '../../store/articles/actions';

import { CSSTransition } from 'react-transition-group';
import { Row, Col, ListGroupItem, Button, ButtonGroup } from 'reactstrap';

import CommentsList from '../CommentsList';

class Article extends Component {
  static propTypes = {
    article: PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      text: PropTypes.string
    }),
    removeArticle: PropTypes.func.isRequired,
  };

  articleContentRef = React.createRef();

  toggleArticleShowBtn = () => {
    const { article, isOpen, toggleOpen } = this.props;

    if(article && article.text) {
      return (
        <Button color="primary"
                outline
                onClick={toggleOpen}>
          {isOpen ? 'Скрыть' : 'Показать'} текст статьи
        </Button>
      )
    }
  }

  removeArticle = () => {
    const { article, removeArticle } = this.props;
    removeArticle(article.id);
  };

  render() {
    const { article, isOpen } = this.props;
    return (
      <ListGroupItem className="Article">
        <Row>
          <Col lg={8}>
            <h3 className="Article__title">
              {article.title}
            </h3>
            <CSSTransition in={isOpen}
                           timeout={400}
                           key={article.id}
                           classNames={{
                             appear: 'transition-appear',
                             appearActive: 'transition-appear-active',
                             enter: 'transition-enter',
                             enterActive: 'transition-enter-active',
                             exit: 'transition-exit',
                             exitActive: 'transition-exit-active',
                           }}
                           appear
                           unmountOnExit>
              <div className="Article__content"
                   ref={this.articleContentRef}>
                <p className="Article__text">
                  {article.text}
                </p>
                <CommentsList comments={article.comments}/>
              </div>
            </CSSTransition>
          </Col>
          <Col className="text-right" lg={4}>
            <ButtonGroup>
              {this.toggleArticleShowBtn()}
              <Button color="danger"
                      onClick={this.removeArticle}
                      outline>
                Удалить
              </Button>
            </ButtonGroup>
          </Col>
        </Row>
      </ListGroupItem>
    );
  }
}

export default connect(null, { removeArticle })(Article);
