import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { Col, ListGroup } from 'reactstrap';

import Article from './Article';

import { filteredArticles } from '../../store/articles/selectors';
import accordionDecorator from '../../decorators/accordion';

class ArticlesList extends Component {
  static propTypes = {
    articles: PropTypes.array,
    openedItemId: PropTypes.number,
    toggleOpen: PropTypes.func.isRequired,
  };

  render() {
    const { articles, openedItemId, toggleOpen } = this.props;
    return (
      <Col lg={12}>
        <ListGroup className="ArticlesList">
          {articles.map(article => (
            <Article article={article}
                     key={article.id}
                     isOpen={article.id === openedItemId}
                     toggleOpen={toggleOpen(article.id)}/>
          ))}
        </ListGroup>
      </Col>
    );
  }
}

export default connect(state => ({
  articles: filteredArticles(state),
}))(accordionDecorator(ArticlesList));
