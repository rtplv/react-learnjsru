import React, { Component } from 'react';
// bootstrap
import { Container, Row } from 'reactstrap';
// components
import Filters from '../components/Filters'
import ArticlesList from '../components/ArticlesList'
// mock
import articles from '../mocks/articles';

class ArticlesView extends Component {
  render() {
    return (
      <Container className="ArticlesView">
        <Row>
          <Filters articles={articles} />
        </Row>
        <Row>
          <ArticlesList defaultOpenId={articles[0].id}/>
        </Row>
      </Container>
    );
  }
}

export default ArticlesView;
